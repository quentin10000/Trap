# Trap

Trap, sorti en 1982 par Nathan, est un jeu de labyrinthe. Le but du jeu est de réussir à sortir du labyrinthe avant la fin du temps et en ayant ramassé le plus de fromages possible avant que les souris ne les mangent. Si une souris passe sur un fromage elle mange un tiers de ce dernier. Au bout de trois passage de souris, le fromage disparaît. De plus, un loup rôde et vous mangera s’il vous croise. Vous pouvez gagner des points en mangeant les fromages et les souris. Plus vous sortirez vite du labyrinthe plus vous gagnerez de points. Si vous vous retrouvez coincé vous avez la possibilité de creuser un trou dans un des murs du labyrinthe. Vous pouvez effectuer cette action jusqu’à trois fois par partie. Plusieurs modes de jeu sont disponibles suivant le niveau de difficulté choisi. Dans les niveaux moyens par exemple, il y aura deux loups au lieu d’un seul. Pour les derniers niveaux, le joueur n’a que quelques secondes pour mémoriser le labyrinthe, puis, il est effacé, laissant le joueur uniquement avec sa mémoire pour trouver la sortie. La partie se termine quand vous sortez du labyrinthe ou lorsque vous vous faites manger par le loup.

## Installation

Pour installer les sources du jeu, suivez les informations suivantes :

```
git clone https://gitlab.com/quentin10000/Trap.git
```

Puis importez le dossier dans Eclipse : Import -> General -> Existing Project into Workspace

## Utilisation

Lancer la classe principal qui ce trouve dans `main\Main.java`

- Flèches directionnelles : déplacer le personnage

- Espace + Flèche directionnelle : faire un trou dans un mur

## Image

![Screenshot](Trap/lib/img/Screenshot_12.png "Trap")

## Bibliothèques

* [LWJGL](http://legacy.lwjgl.org/index.php.html) - Java Game Library
* [Slick2D](http://slick.ninjacave.com/) - 2D Java Game Library

