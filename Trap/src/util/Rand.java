package util;

import java.util.Random;

/**
 *
 * @author meryd
 */
public class Rand {

    /**
     * Importe la classe random.
     */
    private static Random random = new Random();

    /**
     * Renvoie un nombre pseudo-aléatoire compris entre 0 et 2<sup>32</sup>.
     * @return nombre aléatoire compris entre 0 et 2<sup>32</sup>.
     */
    public static int nextInt() {
        return random.nextInt();
    }

    /**
     * Renvoie un nombre pseudo-aléatoire compris entre 0 et limit. 
     * @param limit nombre maximum pour le nombre aléatoire.
     * @return nombre pseudo-aléatoire compris entre 0 et limit.
     */
    public static int nextInt(int limit) {
        return random.nextInt(limit);
    }
}
