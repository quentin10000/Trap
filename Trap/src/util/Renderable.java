package util;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;

/**
 *
 * @author meryd
 */
public interface Renderable {

    /**
     * Affiche si l'objet est affiché ou non.
     *
     * @return si l'objet est modélisé ou non.
     */
    boolean isRendered();
    
    /**
     * Modifie si l'objet est affiché ou non.
     *
     * @param yesno l'objet est affiché ou non.
     */
    void setRendered(boolean yesno);

    /**
     * Renvoie la position x à laquelle l'objet est affiché.
     *
     * @return la position x à laquelle l'objet est affiché.
     */
    int getRenderX();

    /**
     * Renvoie la position y à laquelle l'objet est affiché.
     *
     * @return la position y à laquelle l'objet est affiché.
     */
    int getRenderY();

    /**
     * Effectue le rendu graphique de l'objet.
     *
     * @param gc le conteneur de jeu.
     * @param g le contexte graphique.
     * @throws SlickException
     */
    void render(GameContainer gc, Graphics g) throws SlickException;
}
