package util;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;

/**
 *
 * @author meryd
 */
public interface Updatable {

    /**
     * Affiche si l'objet est mis à jour ou non.
     *
     * @return si l'objet est mis à jour ou non.
     */
    boolean isUpdated();

    /**
     * Modifie si l'objet est mis à jour ou non.
     *
     * @param yesno l'objet est mis à jour ou non.
     */
    void setUpdated(boolean yesno);

    /**
     * Effectue la mise à jour de l'objet.
     *
     * @param gc le conteneur de jeu.
     * @param delta millisecondes écoulées depuis la dernière mise à jour.
     * @throws SlickException
     */
    void update(GameContainer gc, int delta) throws SlickException;
}
