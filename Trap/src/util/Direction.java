package util;

/**
 *
 * @author meryd
 */
public enum Direction {

    /**
     * Les différentes directions possible.
     */
    NORTH(0, -1, 0),
    EAST(1, 0, 1),
    SOUTH(0, 1, 2),
    WEST(-1, 0, 3);
    /**
     * Quantité de déplacement vertical.
     */
    public final int x;
    /**
     * Quantité de déplacement horizontal.
     */
    public final int y;
    /**
     * Place de la direction dans le tableau.
     */
    public final int index;
    /**
     * Tableau des directions.
     */
    private static final Direction[] dirs = {NORTH, EAST, SOUTH, WEST};
    /**
     * Tableau des directions opposées.
     */
    private static final Direction[] opps = {SOUTH, WEST, NORTH, EAST};
    /**
     * Nombre de direction possible.
     */
    public static final int count = dirs.length;

    Direction(int x, int y, int index) {
        this.x = x;
        this.y = y;
        this.index = index;
    }
    
    /**
     * Renvoie la direction correspondant à l'index demandé.
     * @param index index dont on veut savoir la direction correspondante.
     * @return Direction correspondant à l'index demandé.
     */
    public static Direction fromIndex(int index) {
        return dirs[index];
    }

    /**
     * Renvoie l'index correspondant à la direction demandée.
     * @param d direction dont on veut savoir l'index correspondant.
     * @return Index correspondant à la direction demandée
     */
    public static int toIndex(Direction d) {
        return d.index;
    }

    

    /**
     * Renvoie la direction opposée à la direction.
     * @return direction opposée à la direction.
     */
    public Direction opp() {
        return opps[this.index];
    }

}
