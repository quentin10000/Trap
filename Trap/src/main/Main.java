package main;

import org.newdawn.slick.SlickException;
import trap.Trap;

/**
 *
 * @author meryd
 */
public class Main {

    /**
     * Lance Trap.
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) throws SlickException {
        Trap.start();
    }
}
