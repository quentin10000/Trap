package entity;

import maze.Maze;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;

import maze.MazeCell;
import trap.Ressource;

public class Cheese extends Entity {

    /**
     * Nombre de coups qu'il reste à manger dans le fromage.
     */
    private int parts;

    /**
     * Création d'un fromage.
     *
     * @param maze Labyrinthe dans lequel le fromage est créé.
     * @param x Colonne du labyrinthe correspondant à la cellule dans laquelle
     * est le fromage.
     * @param y Ligne du labyrinthe correspondant à la cellule dans laquelle est
     * le fromage.
     */
    public Cheese(Maze maze, int x, int y) {
        super(maze, x, y);
        parts = 3;
    }

    /**
     * Mange une partie du fromage.
     */
    public void eat() {
        parts -= 1;
    }

    /**
     * Affiche le nombre de parties du fromage qu'il reste à manger.
     *
     * @return le nombre de parties du fromage qu'il reste à manger.
     */
    public int getParts() {
        return parts;
    }

    /**
     * Effectue le rendu graphique du vaisseau et des tirs.
     *
     * @param gc le conteneur de jeu.
     * @param g le contexte graphique.
     * @throws SlickException
     */
    @Override
    public void render(GameContainer gc, Graphics g) throws SlickException {
        if (isRendered()) {
            if (parts == 3) {
//                g.setColor(Color.orange);
//                g.fillOval(this.getRenderX(), this.getRenderY(), MazeCell.CELL_WIDTH, MazeCell.CELL_HEIGHT);
                Ressource.cheese3Part.draw(this.getRenderX(), this.getRenderY(), MazeCell.CELL_WIDTH, MazeCell.CELL_HEIGHT);
            }
            if (parts == 2) {
//                g.setColor(Color.orange);
//                g.fillOval(this.getRenderX(), this.getRenderY(), MazeCell.CELL_WIDTH, MazeCell.CELL_HEIGHT);
//                g.setColor(Color.black);
//                g.fillArc(this.getRenderX(), this.getRenderY(), MazeCell.CELL_WIDTH, MazeCell.CELL_HEIGHT, -90, 30);
                Ressource.cheese2Part.draw(this.getRenderX(), this.getRenderY(), MazeCell.CELL_WIDTH, MazeCell.CELL_HEIGHT);
            }
            if (parts == 1) {
//                g.setColor(Color.orange);
//                g.fillOval(this.getRenderX(), this.getRenderY(), MazeCell.CELL_WIDTH, MazeCell.CELL_HEIGHT);
//                g.setColor(Color.black);
//                g.fillArc(this.getRenderX(), this.getRenderY(), MazeCell.CELL_WIDTH, MazeCell.CELL_HEIGHT, -90, 150);
                Ressource.cheese1Part.draw(this.getRenderX(), this.getRenderY(), MazeCell.CELL_WIDTH, MazeCell.CELL_HEIGHT);
            }
        }
    }

}
