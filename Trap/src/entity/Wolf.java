/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;

import maze.Maze;
import maze.MazeCell;
import trap.Ressource;

/**
 *
 * @author Darkcrocs
 */
public class Wolf extends Beast {

    /**
     * Création d'un loup
     *
     * @param maze Labyrinthe dans lequel le loup est créé
     * @param x Colonne du labyrinthe correspondant à la cellule dans laquelle
     * est le loup
     * @param y Ligne du labyrinthe correspondant à la cellule dans laquelle est
     * le loup
     */
    public Wolf(Maze maze, int x, int y) {
        super(maze, x, y);
        speed = 15;
    }

    /**
     * Effectue la mise à jour du loup.
     *
     * @param gc le conteneur de jeu.
     * @param delta millisecondes écoulées depuis la dernière mise à jour.
     * @throws SlickException
     */
    @Override
    public void update(GameContainer gc, int delta) throws SlickException {
        if (isUpdated()) {
            if (time % speed == 0) {
                move(gc, delta);
            }
            time++;

        }
    }

    /**
     * Effectue le rendu graphique du loup.
     *
     * @param gc le conteneur de jeu.
     * @param g le contexte graphique.
     * @throws SlickException
     */
    @Override
    public void render(GameContainer gc, Graphics g) throws SlickException {
        if (isRendered()) {
//            g.setColor(Color.red);
//            g.drawOval(getRenderX(), getRenderY(), MazeCell.CELL_WIDTH, MazeCell.CELL_HEIGHT);
//            g.draw(shape);
            Ressource.wolf.draw(getRenderX(), getRenderY(), MazeCell.CELL_WIDTH, MazeCell.CELL_HEIGHT, Color.red);
        }

    }
}
