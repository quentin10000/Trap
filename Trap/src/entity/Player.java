package entity;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;

import maze.Maze;
import maze.MazeCell;
import trap.CheeseSpawner;
import trap.MouseSpawner;
import trap.Ressource;
import util.Direction;
import util.Timer;

public class Player extends Entity {

    /**
     * Score du joueur.
     */
    private static int score;
    /**
     * Nombre de trou que peu faire le joueur dans le labyrinthe.
     */
    private int drill;
    /**
     * Direction dans laquelle se dirige le joueur.
     */
    private Direction dir;
    /**
     * Temps que le joueur est contant.
     */
    private Timer timer;
    /**
     * Précédent score du joueur.
     */
    private int precedentScore;

    /**
     * Création d'un joueur.
     *
     * @param maze Labyrinthe dans lequel le joueur est créé.
     * @param x Colonne du labyrinthe correspondant à la cellule dans laquelle
     * est le joueur.
     * @param y Ligne du labyrinthe correspondant à la cellule dans laquelle est
     * le joueur.
     */
    public Player(Maze maze, int x, int y) {
        super(maze, x, y);
        score = 0;
        precedentScore = 0;
        drill = 3;
        timer = new Timer(0);
    }

    /**
     * Détruit le mur dans la direction indiqué par le joueur.
     *
     * @param dir direction dans laquelle le joueur veut détruire le mur.
     */
    public void destroyWall(Direction dir) {
        if (drill > 0) {
            if (dir != null && getCurrentCell().getNeighbour(dir) != null && !getCurrentCell().isMerged(dir)) {
                getCurrentCell().merge(dir);
                drill--;
            }
        }
    }

    /**
     * Fait ce qu'il y a à faire lorsque le joueur perd.
     */
    public void GameOver() {
        score = 0;
        stop();

    }

    /**
     * Ajout de points au score.
     *
     * @param add Point de score à ajouter.
     */
    public static void addScore(int add) {
        score += add;
    }

    /**
     * Definir un score.
     *
     * @param set valeur du score.
     */
    public static void setScore(int set) {
        score = set;
    }

    /**
     * Donne la valeur du score.
     *
     * @return Valeur de score.
     */
    public static int getScore() {
        return score;
    }

    /**
     * Effectue le déplacement du joueur dans le labyrinthe.
     *
     * @param gc le conteneur de jeu.
     * @param delta millisecondes écoulées depuis la dernière mise à jour.
     * @throws SlickException
     */
    public void move(GameContainer gc, int delta) throws SlickException {
        Input input = gc.getInput();

        dir = null;

        if (input.isKeyDown(Input.KEY_LEFT)) {
            dir = Direction.WEST;
        }
        if (input.isKeyDown(Input.KEY_RIGHT)) {
            dir = Direction.EAST;
        }
        if (input.isKeyDown(Input.KEY_UP)) {
            dir = Direction.NORTH;
        }
        if (input.isKeyDown(Input.KEY_DOWN)) {
            dir = Direction.SOUTH;
        }
        if (input.isKeyDown(Input.KEY_SPACE) && time % 5 == 0) {
            destroyWall(dir);
        }
        if (time % 5 == 0) {
            if (dir != null && !getCurrentCell().hasWall(dir)) {
                /*
                Si une direction est indiqué et que la cellule actuelle n'a pas 
                de mur dans la direction indiqué, modifie la cellule courante et
                modifie la position du joueur sur l'écran.
                 */
                setCurrentCell(getCurrentCell().c + dir.x, getCurrentCell().l + dir.y);
                this.addRenderX(dir.x * MazeCell.CELL_HEIGHT);
                this.addRenderY(dir.y * MazeCell.CELL_WIDTH);
            }
        }
    }

    /**
     * Effectue la mise à jour du joueur.
     *
     * @param gc le conteneur de jeu.
     * @param delta millisecondes écoulées depuis la dernière mise à jour.
     * @throws SlickException
     */
    @Override
    public void update(GameContainer gc, int delta) throws SlickException {
        if (this.isUpdated()) {
            move(gc, delta);
            /*
            Regarde s'il y a un fromage sur la cellule courante,
            si c'est le cas, le joueur gagne des points et le fromage disparait. 
             */
            Cheese c = CheeseSpawner.getCheese(getCurrentCell());
            if (c != null) {
                addScore(100 * c.getParts());
                CheeseSpawner.removeCheese(c);
            }
            /*
            Regarde s'il y a une souris sur la cellule courante,
            si c'est le cas, le joueur gagne des points et la souris disparait. 
             */
            Mouse m = MouseSpawner.getMouse(getCurrentCell());
            if (m != null) {
                addScore(200);
                MouseSpawner.removeMouse(m);
            }
            timer.update(delta);
            time++;

        }
    }

    /**
     * Effectue le rendu graphique du joueur.
     *
     * @param gc le conteneur de jeu.
     * @param g le contexte graphique.
     * @throws SlickException
     */
    @Override
    public void render(GameContainer gc, Graphics g) throws SlickException {
        if (isRendered()) {
//            g.setColor(Color.blue);
//            g.drawOval(this.getRenderX(), this.getRenderY(), MazeCell.CELL_WIDTH, MazeCell.CELL_HEIGHT);

            /*
            S'il y a un changement de score,
            le joueur est contant pendant 5 secondes.
             */
            if (!timer.isRinging() || precedentScore != score) {
                Ressource.playerHappy.draw(this.getRenderX(), this.getRenderY(), MazeCell.CELL_WIDTH, MazeCell.CELL_HEIGHT);
                if (timer.isRinging()) {
                    timer.reset(5000);
                }

            } else {
                Ressource.player.draw(this.getRenderX(), this.getRenderY(), MazeCell.CELL_WIDTH, MazeCell.CELL_HEIGHT);
            }
            precedentScore = score;
        }
    }

    /**
     * Renvoie le nombre de Drill qu'il reste
     *
     * @return nombre de Drill qu'il reste
     */
    public int getDrill() {
        return drill;
    }

    /**
     * Renvoie la direction dans laquelle le joueur veut aller
     *
     * @return direction dans laquelle le joueur veut aller
     */
    public Direction getDir() {
        return dir;
    }

}
