package entity;

import maze.Maze;
import maze.MazeCell;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;

import util.Renderable;
import util.Updatable;

public abstract class Entity implements Renderable, Updatable {

    /**
     * Controle de la mise a jour de l'entité.
     */
    private boolean updated = true;
    /**
     * Controle du rendu graphique de l'entité.
     */
    private boolean rendered = true;
    /**
     * Affiche l'endroit en x où modeliser l'objet.
     */
    private int renderX;
    /**
     * Affiche l'endroit en y où modeliser l'objet.
     */
    private int renderY;
    /**
     * Labyrinthe dans lequel l'objet est créée.
     */
    private Maze maze;
    /**
     * Cellule dans laquelle l'objet est créée.
     */
    private MazeCell currentCell;
    /**
     * Nombre d'updated fait depuis sa création.
     */
    int time;

    /**
     * Création d'une entitée.
     *
     * @param maze Labyrinthe dans lequel l'entité est créée.
     * @param x Colonne du labyrinthe correspondant à la cellule dans laquelle
     * est l'entité.
     * @param y Ligne du labyrinthe correspondant à la cellule dans laquelle est
     * l'entité.
     */
    public Entity(Maze maze, int x, int y) {
        renderX = x * MazeCell.CELL_HEIGHT;
        renderY = y * MazeCell.CELL_WIDTH;
        currentCell = new MazeCell(maze, x, y);
        this.maze = maze;
        time = 0;
    }

    /**
     * Affiche la cellule dans laquelle est l'objet.
     *
     * @return la cellule dans laquelle est l'objet.
     */
    public MazeCell getCurrentCell() {
        return maze.getCell(currentCell.c, currentCell.l);
    }

    /**
     * Effectue la mise à jour de l'entité.
     *
     * @param gc le conteneur de jeu.
     * @param delta millisecondes écoulées depuis la dernière mise à jour.
     * @throws SlickException
     */
    @Override
    public void update(GameContainer gc, int delta) throws SlickException {
    }

    /**
     * Effectue le rendu graphique de l'entité.
     *
     * @param gc le conteneur de jeu.
     * @param g le contexte graphique.
     * @throws SlickException
     */
    @Override
    public void render(GameContainer gc, Graphics g) throws SlickException {
    }

    /**
     * Affiche si l'objet est mis à jour ou non.
     *
     * @return si l'objet est mis à jour ou non.
     */
    @Override
    public boolean isUpdated() {
        return updated;
    }

    /**
     * Modifie si l'objet est mis à jour ou non.
     *
     * @param yesno l'objet est mis à jour ou non.
     */
    @Override
    public void setUpdated(boolean yesno) {
        updated = yesno;
    }

    /**
     * Affiche si l'objet est affiché ou non.
     *
     * @return si l'objet est modélisé ou non.
     */
    @Override
    public boolean isRendered() {
        return rendered;
    }

    /**
     * Modifie si l'objet est affiché ou non.
     *
     * @param yesno l'objet est affiché ou non.
     */
    @Override
    public void setRendered(boolean yesno) {
        rendered = yesno;
    }

    /**
     * Renvoie la position x à laquelle l'objet est affiché.
     *
     * @return la position x à laquelle l'objet est affiché.
     */
    @Override
    public int getRenderX() {
        return renderX;
    }

    /**
     * Renvoie la position y à laquelle l'objet est affiché.
     *
     * @return la position y à laquelle l'objet est affiché.
     */
    @Override
    public int getRenderY() {
        return renderY;
    }

    /**
     * Ajoute le paramètre x à la précedente valeur de RenderX.
     *
     * @param x valeur à ajouter à la précedente valeur de RenderX.
     */
    public void addRenderX(float x) {
        renderX += x;
    }

    /**
     * Ajoute le paramètre y à la précedente valeur de RenderY.
     *
     * @param y valeur à ajouter à la précedente valeur de RenderY.
     */
    public void addRenderY(float y) {
        renderY += y;
    }

    /**
     * Modifie la valeur de RenderX.
     *
     * @param renderX Nouvelle valeur de RenderX.
     */
    public void setRenderX(int renderX) {
        this.renderX = renderX;
    }

    /**
     * Modifie la valeur de RenderY.
     *
     * @param renderY Nouvelle valeur de RenderY.
     */
    public void setRenderY(int renderY) {
        this.renderY = renderY;
    }

    /**
     * Active la phase cyclique de mise à jour + rendu graphique.
     */
    public void start() {
        setRendered(true);
        setUpdated(true);
    }

    /**
     * Inhibe la phase cyclique de mise à jour + rendu graphique.
     */
    public void stop() {
        setRendered(false);
        setUpdated(false);
    }

    /**
     * Modifie la cellule sur laquelle est modélisée l'objet.
     *
     * @param c Colonne de la cellule.
     * @param l Ligne de la cellule.
     */
    public void setCurrentCell(int c, int l) {
        currentCell = new MazeCell(maze, c, l);
    }

    /**
     * Modifie la cellule sur laquelle est modélisée l'objet.
     *
     * @param c Nouvelle cellule.
     */
    public void setCurrentCell(MazeCell c) {
        currentCell = new MazeCell(maze, c.c, c.l);
    }

}
