package entity;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;

import maze.Maze;
import maze.MazeCell;
import trap.CheeseSpawner;
import trap.Ressource;

public class Mouse extends Beast {

    /**
     * Création d'une souris
     *
     * @param maze Labyrinthe dans lequel la souris est créée
     * @param x Colonne du labyrinthe correspondant à la cellule dans laquelle
     * est la souris
     * @param y Ligne du labyrinthe correspondant à la cellule dans laquelle est
     * la souris
     */
    public Mouse(Maze maze, int x, int y) {
        super(maze, x, y);
        speed = 10;
    }

    /**
     * Effectue la mise à jour de la souris.
     *
     * @param gc le conteneur de jeu.
     * @param delta millisecondes écoulées depuis la dernière mise à jour.
     * @throws SlickException
     */
    @Override
    public void update(GameContainer gc, int delta) throws SlickException {
        if (isUpdated()) {
            if (time % speed == 0) {
                //S'il y a un fromage sur la même case que la souris,
                //mange une part de celui-ci.
                CheeseSpawner.eatCheese(getCurrentCell().c, getCurrentCell().l);
                move(gc, delta);
            }
            time++;
        }
    }

    /**
     * Effectue le rendu graphique de la souris.
     *
     * @param gc le conteneur de jeu.
     * @param g le contexte graphique.
     * @throws SlickException
     */
    @Override
    public void render(GameContainer gc, Graphics g) throws SlickException {
        if (isRendered()) {
//            g.setColor(Color.green);
//            g.drawOval(getRenderX(), getRenderY(), MazeCell.CELL_WIDTH, MazeCell.CELL_HEIGHT);
            Ressource.mouse.draw(getRenderX(), getRenderY(), MazeCell.CELL_WIDTH, MazeCell.CELL_HEIGHT);
        }
    }

}
