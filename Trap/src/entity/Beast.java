package entity;

import java.util.ArrayList;
import maze.Maze;
import maze.MazeCell;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import util.Rand;

import util.Renderable;
import util.Updatable;

public class Beast extends Entity implements Renderable, Updatable {

    /**
     * Cellule précédente sur laquelle l'entité bête était.
     */
    private MazeCell precedentCell;
    /**
     * Vitesse de l'entité bête.
     */
    int speed;

    /**
     * Création d'une entité bête.
     *
     * @param maze Labyrinthe dans lequel l'entitée bête est créée.
     * @param x Colonne du labyrinthe correspondant à la cellule dans laquelle
     * est l'entité bête.
     * @param y Ligne du labyrinthe correspondant à la cellule dans laquelle est
     * l'entité bête.
     */
    public Beast(Maze maze, int x, int y) {
        super(maze, x, y);
        precedentCell = maze.getCell(x, y);
    }

    /**
     * Effectue le déplacement de l'objet dans le labyrinthe.
     *
     * @param gc le conteneur de jeu.
     * @param delta millisecondes écoulées depuis la dernière mise à jour.
     * @throws SlickException
     */
    void move(GameContainer gc, int delta) throws SlickException {
        ArrayList<MazeCell> neighbours = getCurrentCell().getMergedNeighbours();
        //Retire la cellules cellule précédente des choix possible.
        neighbours.remove(precedentCell);
        //Fait une sauvegarde de la cellule courante avant modification.
        MazeCell cell = getCurrentCell();
        //Déplace l'entité bête dans le labyrinthe et ajoute la cellule dans le 
        //chemin parcouru.
        //S'il n'y a pas de voisin (c'est une impasse), fait demi-tour,
        //sinon choisi une cellule aléatoirement.
        setCurrentCell(neighbours.isEmpty() ? precedentCell : neighbours.get(Rand.nextInt(neighbours.size())));
        precedentCell = cell;
        //Déplace l'entité bête sur l'écran
        setRenderX(getCurrentCell().c * MazeCell.CELL_WIDTH);
        setRenderY(getCurrentCell().l * MazeCell.CELL_HEIGHT);
    }

}
