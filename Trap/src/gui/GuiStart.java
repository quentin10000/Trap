package gui;

import java.util.Iterator;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;

import trap.Ressource;
import trap.Trap;

public class GuiStart extends Gui {

    /**
     * Défini si le jeu lancé.
     */
    private static boolean launchGame = false;

    /**
     * Instance du jeu.
     */
    private GuiInGame game;

    /**
     * Creation du menu principal.
     *
     * @param app Contenu du jeu.
     */
    public GuiStart(AppGameContainer app) {
        super(app);
        game = new GuiInGame(app);
    }

    /**
     * Initialisation du gui.
     *
     * @param gc le conteneur de jeu.
     */
    @Override
    public void init(GameContainer gc) {
        this.setBackground(Ressource.guiMainMenu);
        this.setBgScale(0.30F);
        this.addButton(1, getGame().getWidth() / 2 - Ressource.guiButtonPlayBase.getWidth() / 2, 300, 1,
                Ressource.guiButtonPlayBase, Ressource.guiButtonPlayHover);
        this.addButton(2, getGame().getWidth() / 2 - Ressource.guiButtonPlayBase.getWidth() / 2, 400, 1,
                Ressource.guiButtonCloseBase, Ressource.guiButtonCloseHover);
    }

    /**
     * Effectue la mise à jour du gui.
     *
     * @param gc le conteneur de jeu.
     * @param delta millisecondes écoulées depuis la dernière mise à jour.
     * @throws SlickException
     */
    @Override
    public void update(GameContainer gc, int delta) throws SlickException {
        super.update(gc, delta);

        if (!launchGame) {
            Iterator<GuiButton> active = this.getActiveButton();

            while (active.hasNext()) {
                GuiButton button = (GuiButton) active.next();

                int id = button.getId();

                if (id == 1) {
                    launchGame = true;
                    game.init(gc);
                    Trap.setPause(false);
                } else if (id == 2) {
                    System.exit(0);
                }
            }
        } else {
            game.update(gc, delta);
        }
    }

    /**
     * Effectue le rendu graphique du gui.
     *
     * @param gc le conteneur de jeu.
     * @param g le contexte graphique.
     * @throws SlickException
     */
    @Override
    public void render(GameContainer gc, Graphics g) throws SlickException {
        if (!launchGame) {
            clear();
            super.render(gc, g);
            //Ressource.androids.draw(getGame().getWidth() / 2 - Ressource.androids.getWidth() / 2, 50);
            g.drawString("Projet RT 2016/2017", 10, getGame().getHeight() - 45);
            g.drawString("Réalisé par Quentin, Marion, Tristan, Sayd", 10, getGame().getHeight() - 25);
        } else {
            game.render(gc, g);
        }

    }

    /**
     * Défini si le jeu est launcé ou pas
     *
     * @param bool True : jeu lancé<br>
     * False : jeu non lancé
     */
    public static void setLaunchGame(boolean bool) {
        launchGame = bool;
    }

}
