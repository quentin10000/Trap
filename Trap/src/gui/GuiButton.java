package gui;

import org.lwjgl.input.Mouse;
import org.newdawn.slick.Image;
import trap.Trap;

public class GuiButton {

    /**
     * Image de base du button
     */
    private Image base;

    /**
     * Image de hover du button (quand on passe la souris dessus)
     */
    private Image hover;

    /**
     * Image actuellement afficher du bouton
     */
    private Image current;

    /**
     * Position en x du bouton
     */
    private int x;

    /**
     * Position en y du bouton
     */
    private int y;

    /**
     * Identifiant du bouton
     */
    private int id;

    /**
     * Facteur de taille du bouton
     */
    private float scale;

    /**
     * Actif si la souris clique dessus
     */
    private boolean active = false;

    /**
     * Créer un bouton
     *
     * @param id identifiant du bouton
     * @param x position en x du bouton
     * @param y position en y du bouton
     * @param scale facteur de taille du bouton
     * @param base img de base du bouton
     * @param hover img de hover du bouton (quand on passe la souris dessus)
     */
    public GuiButton(int id, int x, int y, float scale, Image base, Image hover) {
        this.base = base;
        this.hover = hover;
        this.x = x;
        this.y = y;
        this.id = id;
        this.scale = scale;
        this.current = base;
    }

    /**
     * Mise a jour du bouton
     */
    public void update() {
        int mouseX = Mouse.getX();
        int mouseY = Trap.DISPLAY_HEIGHT - Mouse.getY();

        active = false;
        //si la souris est sur le bouton
        if (mouseX > this.x && mouseX < this.x + this.base.getWidth() * scale && mouseY > this.y
                && mouseY < this.y + this.base.getHeight() * scale) {
            //si le joueur à cliqué sur le bouton
            if (Mouse.isButtonDown(0)) {
                active = true;
            }
            current = hover;
        } else {
            current = base;
        }
    }

    /**
     * Affichage du bouton
     */
    public void render() {
        current.draw(x, y, scale);
    }

    /**
     * Vérifie si le bouton est cliquer ou pas
     *
     * @return True : si le bouton est cliquer False : si le bouton n'est pas
     * cliquer
     */
    public boolean isClicked() {
        return active;
    }

    /**
     * Donne l'identifiant du bouton
     *
     * @return Identifiant du bouton
     */
    public int getId() {
        return id;
    }

}
