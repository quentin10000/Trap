package gui;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import trap.Trap;

import trap.Ressource;

public class Gui {

    /**
     * Contenu du jeu
     */
    private AppGameContainer game;

    /**
     * Liste des buttons affichés
     */
    private List<GuiButton> buttons = new ArrayList<GuiButton>();

    /**
     * Liste des bouttons cliquer
     */
    private List<GuiButton> actives = new ArrayList<GuiButton>();

    /**
     * Image de fond
     */
    private Image bg;

    /**
     * Position en x de l'image de fond
     */
    private int bgX;

    /**
     * Position en y de l'image de fond
     */
    private int bgY;

    /**
     * Facteur de taille de l'image fond
     */
    private float bgScale = 1;

    /**
     * Temps écoulé depuis la création du Gui
     */
    private int time = 0;

    /**
     * Temps sauvegardé
     */
    private int saveTime = 0;

    /**
     * Créer un menu
     *
     * @param app Contenu du jeu
     */
    public Gui(AppGameContainer app) {
        game = app;
    }

    /**
     * Vide la scène du jeu
     */
    public void clear() {
        game.getGraphics().clear();
    }

    /**
     * Fonction d'initialisation du menu
     *
     * @param gc Contenu du jeu
     * @throws SlickException
     */
    public void init(GameContainer gc) throws SlickException {
    }

    /**
     * Mise à jour du contenu du menu
     *
     * @param gc Contenu du jeu
     * @param delta Temps en milisec depuis la dernière frame
     * @throws SlickException
     */
    public void update(GameContainer gc, int delta) throws SlickException {
        actives.removeAll(actives);
        for (GuiButton button : buttons) {
            button.update();
            if (button.isClicked() && !actives.contains(button) && saveTime < time) {
                actives.add(button);
                saveTime = time + 60;
            }
        }
        time++;
    }

    /**
     * Rendu du menu
     *
     * @param gc Contenu du jeu
     * @param g Elément de dessin
     * @throws SlickException
     */
    public void render(GameContainer gc, Graphics g) throws SlickException {
        if (bg != null) {
            bg.draw(bgX, bgY, bgScale);
        }
        for (GuiButton button : buttons) {
            button.render();
        }

    }

    /**
     * Affiche du texte à l'écran
     *
     * @param str Texte à afficher
     * @param x Position en x du texte
     * @param y Position en y du texte
     */
    public void drawString(String str, int x, int y) {
        Ressource.lemonMick.drawString(x, y, str);
    }

    /**
     * Ajouter un bouton sur le menu
     *
     * @param id identifiant du bouton
     * @param x position en x du bouton
     * @param y position en y du bouton
     * @param scale facteur de taille du bouton
     * @param base img de base du bouton
     * @param hover img de hover du bouton (quand on passe la souris dessus)
     */
    public void addButton(int id, int x, int y, float scale, Image base, Image hover) {
        buttons.add(new GuiButton(id, x, y, scale, base, hover));
    }

    /**
     * Défini une image de fond pour le menu
     *
     * @param img Image de fond à appliquer
     */
    public void setBackground(Image img) {
        bg = img;
        bgX = Trap.DISPLAY_WIDTH / 2 - bg.getWidth() / 2;
        bgY = Trap.DISPLAY_HEIGHT / 2 - bg.getHeight() / 2;
    }

    /**
     * Definir un facteur de taille de l'image de fond
     *
     * @param bgscale facteur de taille
     */
    public void setBgScale(float bgscale) {
        bgX = (int) (Trap.DISPLAY_WIDTH / 2 - bg.getWidth() * bgscale / 2);
        bgY = (int) (Trap.DISPLAY_HEIGHT / 2 - bg.getHeight() * bgscale / 2);
        this.bgScale = bgscale;
    }

    /**
     * Donne tous les boutons sur lesquels on a cliqué
     *
     * @return Liste de bouttons
     */
    public Iterator<GuiButton> getActiveButton() {
        return actives.iterator();
    }

    /**
     * Donne le contenu du jeu
     *
     * @return Le jeu
     */
    public AppGameContainer getGame() {
        return game;
    }

}
