package gui;

import entity.Player;
import entity.Wolf;
import java.util.Iterator;
import maze.Maze;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import trap.CheeseSpawner;
import trap.MouseSpawner;
import trap.Ressource;
import trap.Trap;
import util.Direction;
import util.Timer;

public class GuiInGame extends Gui {

    /**
     * Labyrinthe qu'il contient.
     */
    private Maze maze;
    /**
     * Créateur de souris.
     */
    private MouseSpawner sm;
    /**
     * Joueur.
     */
    private Player player;
    /**
     * Créateur de fromages.
     */
    private CheeseSpawner sc;
    /**
     * Loup.
     */
    private Wolf wolf;
    /**
     * La partie est gagnée.
     */
    private Boolean won;
    /**
     * Temps avant d'avoir perdu la partie.
     */
    private Timer time2;

    /**
     * Nombre d'update fait depuis sa création.
     */
    private int time;

    /**
     * Le jeu est en game over.
     */
    private boolean gameOver;

    /**
     * Position y des image de fond.
     */
    private int y = 0;

    /**
     * Création de l'interface en jeu.
     *
     * @param app Contenu du jeu.
     */
    public GuiInGame(AppGameContainer app) {
        super(app);
    }

    /**
     * Initialisation du jeu et des guis.
     *
     * @param gc le conteneur de jeu.
     */
    @Override
    public void init(GameContainer gc) {
        gameOver = false;
        won = false;
        maze = new Maze(210, 100);
        sc = new CheeseSpawner(maze, 5);
        sm = new MouseSpawner(maze, 3);
        player = new Player(maze, maze.getInitCell().c, maze.getInitCell().l);
        wolf = new Wolf(maze, maze.getExitCell().c, maze.getExitCell().l);
        time2 = new Timer(300000);
        maze.start();

        this.addButton(1, getGame().getWidth() / 2 - Ressource.guiButtonPauseBase.getWidth() / 2, 300, 1,
                Ressource.guiButtonPauseBase, Ressource.guiButtonPauseHover);
        this.addButton(2, getGame().getWidth() / 2 - Ressource.guiButtonMainBase.getWidth() / 2, 400, 1,
                Ressource.guiButtonMainBase, Ressource.guiButtonMainHover);
    }

    /**
     * Affiche le loup du jeu.
     *
     * @return le loup du jeu.
     */
    public Wolf getWolf() {
        return wolf;
    }

    /**
     * Effectue la mise à jour du jeu et des guis.
     *
     * @param gc le conteneur de jeu.
     * @param delta millisecondes écoulées depuis la dernière mise à jour.
     * @throws SlickException
     */
    @Override
    public void update(GameContainer gc, int delta) throws SlickException {
        //Si trap est en pause.
        if (!Trap.isPaused()) {
            sm.update(gc, delta);
            wolf.update(gc, delta);
            player.update(gc, delta);

        } else {
            Iterator<GuiButton> active = this.getActiveButton();

            while (active.hasNext()) {
                GuiButton button = (GuiButton) active.next();

                int id = button.getId();

                if (id == 1) {
                    Trap.setPause(false);
                } else if (id == 2) {
                    GuiStart.setLaunchGame(false);
                    gameOver = true;
                }
            }
        }
        Input input = gc.getInput();
        //Si le joueur et le loup sont sur la même case : game over.
        if (player.getCurrentCell() == wolf.getCurrentCell()) {
            gameOver = true;
        }
        //Lorsque le joueur a perdu.
        if (gameOver) {
            player.GameOver();
            wolf.stop();
            maze.stop();
            sm.stop();
            sc.stop();
        }
        //Lorsque le joueur se trouve sur la case de sortie
        //et qu'il sort du labyrinthe
        if (player.getCurrentCell() == maze.getExitCell() && player.getDir() == Direction.EAST) {
            won = true;
        }
        //Lorsque le joueur a gagné.
        if (won) {
            player.stop();
            wolf.stop();
            maze.stop();
            sm.stop();
            sc.stop();
        }
        //Touche de débug
        if (input.isKeyPressed(Input.KEY_F3)) {
            Trap.DEBUG = !Trap.DEBUG;
        }
        //Lorsque le joueur veut recommencer le jeu.
        if ((gameOver || won) && input.isKeyDown(Input.KEY_R)) {
            init(gc);
        }
        //Le joueur veut mettre le jeu en pause.
        if (input.isKeyPressed(Input.KEY_ESCAPE)) {
            // pause = !pause;
            Trap.setPause(!Trap.isPaused());
        }
        time++;
        //Tant que le joueur n'a pas gagné.
        if (!won) {
            time2.update(delta);
        }
        //Si le joueur met plus de 300 secondes
        //à trouver la sortie du labyrinthe.
        if (time2.isRinging()) {
            gameOver = true;
        }
        super.update(gc, delta);
    }

    /**
     * Effectue le rendu graphique du jeu et des guis.
     *
     * @param gc le conteneur de jeu.
     * @param g le contexte graphique.
     * @throws SlickException
     */
    @Override
    public void render(GameContainer gc, Graphics g) throws SlickException {
        if (y > Trap.GAME_HEIGHT) {
            y = 0;
        }

        y++;

        int gameW = Trap.GAME_WIDTH;
        int gameH = Trap.GAME_HEIGHT;
        int gameX = (int) (0.5f * (Trap.DISPLAY_WIDTH - gameW));
        int gameY = Trap.DISPLAY_HEIGHT - Trap.GAME_HEIGHT;
        g.setClip(gameX, gameY, gameW, gameH);
        maze.render(gc, g);
        sm.render(gc, g);
        sc.render(gc, g);
        player.render(gc, g);
        wolf.render(gc, g);
        //Quoi afficher si le joueur à perdu.
        if (gameOver) {
            int margeTop = 290;

            Ressource.lemonMick.drawString(Trap.DISPLAY_WIDTH / 2 - 120, margeTop, "Game Over", Color.red);

            String score = "Score final : " + Player.getScore();
            Ressource.lemonMick.drawString(Trap.DISPLAY_WIDTH / 2 - (score.length() / 2 * 22), margeTop + 50,
                    score);
            Ressource.lemonMick.drawString(Trap.DISPLAY_WIDTH / 2 - 200, margeTop + 100, "Press R to restart");
            //Quoi afficher si le joueur à gagné.
        } else if (won) {
            int margeTop = 290;
            Ressource.lemonMick.drawString(Trap.DISPLAY_WIDTH / 2 - 120, margeTop, "Bravo !", Color.red);

            String score = "Score final : " + (Player.getScore() + (time2.getDelay() / 1000 - time2.getTicks() / 1000));

            Ressource.lemonMick.drawString(Trap.DISPLAY_WIDTH / 2 - (score.length() / 2 * 22), margeTop + 50,
                    score);
            Ressource.lemonMick.drawString(Trap.DISPLAY_WIDTH / 2 - 200, margeTop + 100, "Press R to restart");
            //Quoi afficher si le joueur joue.
        } else {
            Ressource.lemonMick.drawString(90, 715, "Drill : " + player.getDrill());
            Ressource.lemonMick.drawString(90, 650, "Score : " + Player.getScore());
            Ressource.lemonMick.drawString(300, 715, "Time : " + (time2.getDelay() / 1000 - time2.getTicks() / 1000));
        }
        //Quoi afficher si le joueur est en débug.
        if (Trap.DEBUG) {
            gc.setShowFPS(true);
            g.setColor(Color.white);
            g.drawString("Drill : " + player.getDrill(), 10.0f, 30.0f);
            g.drawString("Score : " + Player.getScore(), 10.0f, 50.0f);
            g.drawString("Time : " + (time2.getDelay() / 1000 - time2.getTicks() / 1000), 10.0f, 70.0f);
        }
        //Quoi afficher si le joueur est en pause.
        if (Trap.isPaused()) {
            g.setColor(new Color(0.1F, 0.1F, 0.1F, 0.7F));
            g.fillRect(0, 0, Trap.DISPLAY_WIDTH, Trap.DISPLAY_HEIGHT);
            super.render(gc, g);
        }
    }
}
