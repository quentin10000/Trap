package maze;

import java.util.ArrayList;
import java.util.List;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import util.Direction;
import util.Rand;
import util.Renderable;
import trap.Trap;

public class Maze implements Renderable {

    /**
     * Nombre de colonnes du labyrinthe par défaut.
     */
    public static final int MAZE_COLUMNS = 15;
    /**
     * Nombre de lignes du labyrinthe par défaut.
     */
    public static final int MAZE_LINES = 8;
    /**
     * Nombre de colonnes du labyrinthe minimum.
     */
    public static final int MAZE_MIN_COLUMNS = 5;
    /**
     * Nombre de lignes du labyrinthe minimum.
     */
    public static final int MAZE_MIN_LINES = 5;
    /**
     * Nombre de colonnes du labyrinthe maximum.
     */
    public static final int MAZE_MAX_COLUMNS = Trap.DISPLAY_WIDTH / MazeCell.CELL_WIDTH;
    /**
     * Nombre de lignes du labyrinthe maximum.
     */
    public static final int MAZE_MAX_LINES = (Trap.DISPLAY_HEIGHT - 100) / MazeCell.CELL_HEIGHT;

    /**
     * Nombre de colonnes du labyrinthe.
     */
    public final int columns;
    /**
     * Nombre de lignes du labyrinthe.
     */
    public final int lines;
    /**
     * Taille du labyrinthe.
     */
    public final int size;

    /**
     * Controle du rendu graphique du labyrinthe.
     */
    private boolean rendered;
    /**
     * Affiche l'endroit en x où modeliser l'objet.
     */
    private int renderX;
    /**
     * Affiche l'endroit en y où modeliser l'objet.
     */
    private int renderY;
    /**
     * Tableau des cellules constituant le labyrinthe.
     */
    private MazeCell[] cells;
    /**
     * Tableau des cellules impasses.
     */
    private ArrayList<MazeCell> deadEnds = new ArrayList<MazeCell>();
    /**
     * Cellule d'entrée du labyrinthe.
     */
    private MazeCell initCell;
    /**
     * Cellule de sortie du labyrinthe.
     */
    private MazeCell exitCell;

    /**
     * Construit un labyrinthe avec les valeurs par défaut.
     */
    public Maze() {
        this(MAZE_COLUMNS, MAZE_LINES);
    }

    /**
     * Construit un labyrinthe avec nbCols colonnes et nbLines lignes.
     *
     * @param nbCols nombre de colonnes du labyrinthe.
     * @param nbLines nombre de lignes du labyrinthe.
     */
    public Maze(int nbCols, int nbLines) {
        if (nbCols < MAZE_MIN_COLUMNS) {
            columns = MAZE_MIN_COLUMNS;
        } else if (nbCols > MAZE_MAX_COLUMNS) {
            columns = MAZE_MAX_COLUMNS;
        } else {
            columns = nbCols;
        }

        if (nbLines < MAZE_MIN_LINES) {
            lines = MAZE_MIN_LINES;
        } else if (nbLines > MAZE_MAX_LINES) {
            lines = MAZE_MAX_LINES;
        } else {
            lines = nbLines;
        }

        size = columns * lines;
        cells = new MazeCell[size];
        int k = 0;
        for (int i = 0; i < columns; i++) {
            for (int j = 0; j < lines; j++) {
                cells[j * columns + i] = new MazeCell(this, i, j);
                k++;
            }
        }
        generateRemake();
        rendered = true;

    }

    /**
     * Fait la fusion entre la cellule cell et la cellule dans la direction d.
     *
     * @param cell cellule actuelle.
     * @param d direction de la cellule voisine.
     */
    private void merge(MazeCell cell, Direction d) {
        // Require: cell has a neighbour in direction d !!
        cell.lowerWall(d);
        cell = cell.getNeighbour(d);
        cell.lowerWall(d.opp());
    }

    /**
     * Renvoie le nombre de direction où on peut fusionner.
     *
     * @param cell cellule actuelle.
     * @param mergeDirs liste des direction.
     * @return le nombre de direction où on peut fusionner.
     */
    private int nextMergeDirs(MazeCell cell, Direction[] mergeDirs) {
        int mergeDirsSize = 0;
        for (Direction d : Direction.values()) {
            MazeCell neighbour = cell.getNeighbour(d);
            if ((neighbour != null) && (!neighbour.isVisited()) && cell.canMerge(d)) {
                // This direction is ok for merge
                mergeDirs[mergeDirsSize] = d;
                mergeDirsSize++;
            }
        }
        // System.out.println(mergeDirsSize);
        return mergeDirsSize;
        // return Rand.nextInt(mergeDirsSize);
    }

    /**
     * Est censé s'occuper de la génération du labyrinthe (mais ne fonctionne pas).
     */
    private void generate() {
        // Choose initial cell
        initCell = getCell(0, Rand.nextInt(lines));
        // Allocate cell stack for choices
        MazeCell[] cellStack = new MazeCell[size];
        int stackSize = 0;
        // Allocate dead ends stack
        MazeCell[] deadEnds = new MazeCell[size];
        int deadEndsSize = 0;

        MazeCell currentCell = initCell;
        initCell.lowerWall(Direction.EAST);

        Direction[] mergeDirs = new Direction[Direction.count];
        int mergeDirsSize;

        // Loop until the maze if fully generated
        while (true) {
            // First mark the current as visited
            currentCell.markAsVisited();
            // Find all unvisited and unmerged neighbours (if any)
            mergeDirsSize = nextMergeDirs(currentCell, mergeDirs);

            if (mergeDirsSize < 1) {
                // Case 1: There is no unvisited and unmerged neighbour
                if (stackSize > 0) {
                    /*
					 * Case 1.1: The cell stack is not empty, therefore we still
					 * have previously encountered choice points from which we
					 * might restart the dig and merge process
                     */
                    stackSize--;
                    currentCell = cellStack[stackSize];
                } else {
                    // Case 1.2: The cell stack is empty, process is over.
                    break;
                }
            } else {
                // Case 2: There is at least one unmerged unvisited neighbour
                // Randomly choose one and merge it with current cell
                Direction mergeDir = mergeDirs[Rand.nextInt(mergeDirsSize)];
                merge(currentCell, mergeDir);
                /*
				 * If there were more than one neighbour, we were forced to make
				 * a random choice. Therefore, we must keep in mind that we
				 * might reconsider this cell later.
                 */
                // if (mergeDirsSize > 1) {
                // Put the cell on the stack
                cellStack[stackSize] = currentCell;
                stackSize++;
                // }
                // Update the current cell before looping again
                currentCell = currentCell.getNeighbour(mergeDir);
            }
        }
    }

    /**
     * S'occupe de la génération du labyrinthe.
     */
    private void generateRemake() {
        //Choisis la cellule de sortie du labyrinthe.
        exitCell = getCell(columns - 1, Rand.nextInt(lines));
        //Choisis la cellule d'entrée du labyrinthe.
        initCell = getCell(0, Rand.nextInt(lines));

        exitCell.lowerWall(Direction.EAST);

        exitCell.setBackgroundColor(Color.green);
        initCell.setBackgroundColor(Color.red);
        //On commence sur la cellule d'entrée.
        MazeCell currentCell = initCell;
        //Liste des cellules qui ont encore des cellules voisines non visitées.
        List<MazeCell> stack = new ArrayList<MazeCell>();

        // On fait la boucle jusqu'à ce que le labyrinthe soit fait entièrement.
        while (true) {
            // D'abord on marque la cellule courante comme visitée.
            currentCell.markAsVisited();
            // On trouve toutes les cellules voisines qui n'ont pas été visitées
            // et qui existent.
            List<Direction> dirs = new ArrayList<Direction>();
            for (Direction d : Direction.values()) {
                MazeCell neighbour = currentCell.getNeighbour(d);
                if (neighbour != null && (!neighbour.isVisited())) {
                    dirs.add(d);
                }
            }
            //S'il n'y a pas de cellules voisines non visitées.
            if (dirs.isEmpty()) {
                //S'il y a encore des cellules dont les cellules voisines
                //n'ont pas été visitées et que la liste a au moins 2 éléments : 
                //on prend l'avant-dernière cellule de la liste
                //(en supprimant la dernière ce celle-ci).
                if (!stack.isEmpty() && stack.size() > 1) {
                    MazeCell last = stack.get(stack.size() - 1);
                    stack.remove(last);
                    currentCell = stack.get(stack.size() - 1);
                    continue;
                    //S'il n'y a plus qu'une cellule maximum dans la liste :
                    //le labyrinthe est completement généré.
                } else {
                    break;
                }
                //S'il y a des cellules voisines non visitées.
            } else {
                //On choisit une direction au hasard
                Direction select = dirs.get(Rand.nextInt(dirs.size()));
                //On fusionne les cellules dans la direction choisie
                merge(currentCell, select);
                currentCell = currentCell.getNeighbour(select);
            }
            //On ajoute la cellule voisine à celle qui viens d'être fusionnée
            //à la liste des cellules qui ont encore des cellules non visitées.
            stack.add(currentCell);

        }
        //Une fois que le labyrinthe est généré,
        //on regarde quelle cellule est une impasse et on l'ajoute à la liste.
        for (MazeCell mazeCell : cells) {
            if (mazeCell.isDeadEnd()) {
                deadEnds.add(mazeCell);
            }
        }
    }

    /**
     * Marquer une cellule comme non visitée.
     */
    private void setUnvisitedMaze() {
        for (MazeCell mazeCell : cells) {
            mazeCell.markAsUnvisited();
        }
    }

    /**
     * Change la couleur du fond de chaque cellule du labyrinthe.
     *
     * @param color Nouvelle couleur.
     */
    public void changeColor(Color color) {
        for (MazeCell mazeCell : cells) {
            mazeCell.setBackgroundColor(color);
        }
    }

    /**
     * Trouve le chemin dans le labyrinthe entre une cellule 1(x,y) et une cellule2(x1,x1).
     *
     * @param x colonne de la cellule 1
     * @param y ligne de la cellule 1
     * @param x1 colonne de la cellule 2
     * @param y1 ligne de la cellule 2
     * @return la liste des cellules qu'il faut traverser pour arriver à la cellule 2 à partir de la cellule 1.
     */
    public List<MazeCell> getPath(int x, int y, int x1, int y1) {
        List<MazeCell> way = new ArrayList<MazeCell>();
        boolean found = false;

        MazeCell startCell = getCell(x, y);
        MazeCell endCell = getCell(x1, y1);
        MazeCell currentCell = startCell;

        ArrayList<MazeCell> stack = new ArrayList<MazeCell>();

        setUnvisitedMaze();

        while (!found) {

            currentCell.markAsVisited();

            if (currentCell == endCell) {
                found = true;
                break;
            }

            List<Direction> dirs = new ArrayList<Direction>();
            for (Direction d : Direction.values()) {

                MazeCell neighbour = currentCell.getNeighbour(d);
                if (neighbour != null && !neighbour.isVisited() && !currentCell.hasWall(d)) {
                    dirs.add(d);
                }
            }

            if (dirs.isEmpty()) {
                if (!stack.isEmpty() && stack.size() > 1) {
                    MazeCell last = stack.get(stack.size() - 1);
                    stack.remove(last);
                    currentCell = stack.get(stack.size() - 1);
                    continue;
                } else {
                    break;
                }
            } else {
                Direction select = dirs.get(Rand.nextInt(dirs.size()));
                currentCell = currentCell.getNeighbour(select);
            }
            stack.add(currentCell);
        }

        return stack;

    }

    /**
     * Renvoie la cellule à la colonne c et à la ligne l.
     *
     * @param c colonne c.
     * @param l ligne l.
     * @return la cellule à la colonne c et à la ligne l.
     */
    public MazeCell getCell(int c, int l) {
        if ((c < 0 || c >= columns) || (l < 0 || l >= lines)) {
            return null;
        } else {
            return cells[l * columns + c];
        }
    }

    /**
     * Renvoie la cellule numéro n.
     *
     * @param n numéro de la cellule.
     * @return la cellule numéro n.
     */
    public MazeCell getCell(int n) {
        if ((n < 0) || (n >= size)) {
            return null;
        }
        return cells[n];
    }

    /**
     * Renvoie la cellule d'entrée du labyrinthe.
     *
     * @return la cellule d'entrée du labyrinthe.
     */
    public MazeCell getInitCell() {
        return initCell;
    }

    /**
     * Renvoie la cellule de sortie du labyrinthe.
     *
     * @return la cellule de sortie du labyrinthe.
     */
    public MazeCell getExitCell() {
        return exitCell;
    }

    /**
     * Renvoie aléatoirement une cellule.
     *
     * @return une cellule aléatoire.
     */
    public MazeCell getRandomCell() {
        return cells[Rand.nextInt(size)];
    }

    /**
     * Renvoie aléatoirement une cellule à la colonne c.
     *
     * @param c colonne c.
     * @return une colonne aléatoire.
     */
    public MazeCell getRandomCellInColumn(int c) {
        return cells[c * Rand.nextInt(lines)];
    }

    /**
     * Renvoie aléatoirement une cellule à la ligne l.
     *
     * @param l ligne l.
     * @return une ligne aléatoire.
     */
    public MazeCell getRandomCellInLine(int l) {
        return cells[l * Rand.nextInt(columns)];
    }

    /**
     * Renvoie aléatoirement une impasse.
     *
     * @return une impasse aléatoire.
     */
    public MazeCell getRandomDeadEnd() {
        return deadEnds.get(Rand.nextInt(deadEnds.size() - 1));
    }

    /**
     * Renvoie les impasses du labyrinthe en liste.
     *
     * @return impasses du labyrinthe en liste.
     */
    public List<MazeCell> getDeadEndsAsList() {
        return deadEnds;
    }

    /**
     * Renvoie les impasses du labyrinthe en tableau.
     *
     * @return impasses du labyrinthe en tableau.
     */
    public MazeCell[] getDeadEndsAsArray() {
        return (MazeCell[]) deadEnds.toArray();
    }

    public void openExit() {
        // TODO
    }

    public void print() {
        // TODO
    }

    /**
     * Affiche si l'objet est affiché ou non.
     *
     * @return si l'objet est modélisé ou non.
     */
    @Override
    public boolean isRendered() {
        return rendered;
    }

    /**
     * Modifie si l'objet est affiché ou non.
     *
     * @param yesno l'objet est affiché ou non.
     */
    @Override
    public void setRendered(boolean yesno) {
        rendered = yesno;
    }

    /**
     * Effectue le rendu graphique du labyrinthe.
     *
     * @param gc le conteneur de jeu.
     * @param g le contexte graphique.
     * @throws SlickException
     */
    @Override
    public void render(GameContainer gc, Graphics g) {
        if (isRendered()) {
            for (MazeCell mazeCell : cells) {
                if (mazeCell != null) {
                    mazeCell.render(gc, g);
                }
            }
        }
    }

    /**
     * Renvoie la position renderX à laquelle l'objet est affiché.
     *
     * @return la position renderX à laquelle l'objet est affiché.
     */
    @Override
    public int getRenderX() {
        return renderX;
    }

    /**
     * Renvoie la position renderY à laquelle l'objet est affiché.
     *
     * @return la position renderY à laquelle l'objet est affiché.
     */
    @Override
    public int getRenderY() {
        return renderY;
    }

    /**
     * Renvoie la position renderW à laquelle l'objet est affiché est fini
     * d'être affiché.
     *
     * @return renderW à laquelle l'objet est affiché est fini d'être affiché.
     */
    public float getRenderW() {
        return renderX * MazeCell.CELL_WIDTH;
    }

    /**
     * Renvoie la position renderH à laquelle l'objet est affiché est fini
     * d'être affiché.
     *
     * @return renderH à laquelle l'objet est affiché est fini d'être affiché.
     */
    public float getRenderH() {
        return renderY * MazeCell.CELL_HEIGHT;
    }

    /**
     * Modifie la valeur de RenderX.
     *
     * @param renderX Nouvelle valeur de RenderX.
     */
    public void setRenderX(int renderX) {
        this.renderX = renderX;
    }

    /**
     * Modifie la valeur de RenderY.
     *
     * @param renderY Nouvelle valeur de RenderY.
     */
    public void setRenderY(int renderY) {
        this.renderY = renderY;
    }

    /**
     * Modifie la valeur de RenderX et RenderY.
     *
     * @param renderX Nouvelle valeur de RenderX.
     * @param renderY Nouvelle valeur de RenderY.
     */
    public void setRenderXY(int renderX, int renderY) {
        this.renderX = renderX;
        this.renderY = renderY;
    }

    /**
     * Active la phase cyclique de mise à jour + rendu graphique.
     */
    public void start() {
        setRendered(true);
    }

    /**
     * Inhibe la phase cyclique de mise à jour + rendu graphique.
     */
    public void stop() {
        setRendered(false);
    }
}
