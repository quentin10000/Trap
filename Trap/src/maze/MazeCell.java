package maze;

import java.util.ArrayList;
import java.util.List;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import trap.Ressource;
import util.Direction;
import util.Renderable;

public class MazeCell implements Renderable {

    /**
     * Largeur d'une cellule en pixel.
     */
    public static final int CELL_WIDTH = 64;
    /**
     * Hauteur d'une cellule en pixel.
     */
    public static final int CELL_HEIGHT = 64;

    /**
     * Labyrinthe qui contient la cellule.
     */
    public final Maze maze;
    /**
     * Numéro de colonne de la cellule dans le labyrinthe.
     */
    public final int c;
    /**
     * Numéro de ligne de la cellule dans le labyrinthe.
     */
    public final int l;
    /**
     * Tableau de l'état des murs de la cellule.
     */
    private boolean[] walls;
    /**
     * Largeur des murs.
     */
    private int wallWidth;
    /**
     * Controle du rendu graphique de la cellule.
     */
    private boolean rendered;
    /**
     * Couleur du fond de la cellule.
     */
    private Color bg;

    /**
     * Marqueur indiquant si la cellule à été visitée.
     */
    private boolean visited;

    /**
     * Construit une cellule du labyrinthe maze à l'emplacement (c,l).
     * @param maze Labyrinthe qui contient la cellule.
     * @param c Numéro de colonne de la cellule dans le labyrinthe.
     * @param l Numéro de ligne de la cellule dans le labyrinthe.
     */
    public MazeCell(Maze maze, int c, int l) {
        this.maze = maze;
        this.c = c;
        this.l = l;
        this.visited = false;
        this.rendered = true;
        wallWidth = 4;
        this.bg = Color.white;
        this.walls = new boolean[Direction.count];
        for (Direction d : Direction.values()) {
            this.walls[d.index] = true; // raiseWall(d);
        }
    }

    /**
     * Regarde si la cellule est visitée.
     * @return booléen disant si la cellule est visitée.
     */
    boolean isVisited() {
        return visited;
    }

    /**
     * Marque la cellule comme visitée.
     */
    void markAsVisited() {
        visited = true;
    }

    /**
     * Marque la cellule comme non visitée.
     */
    void markAsUnvisited() {
        visited = false;
    }

    /**
     * Modifie l'état du mur de la direction d.
     * @param d direction où on veut modifier l'état du mur.
     * @param upper nouvel état du mur.
     */
    public void setWall(Direction d, boolean upper) {
        walls[d.index] = upper;
    }

    /**
     * Lève le mur dans la direction d.
     * @param d direction où on veut lever le mur.
     */
    public void raiseWall(Direction d) {
        walls[d.index] = true;
    }

    /**
     * Baisse le mur dans la direction d.
     * @param d direction où on veut baisser le mur.
     */
    public void lowerWall(Direction d) {
        walls[d.index] = false;
    }

    /**
     * Indique si la cellule a un mur levé dans la direction d.
     * @param d direction dans laquelle on veut savoir si le mur est levé.
     * @return booléen disant si le mur est levé.
     */
    public boolean hasWall(Direction d) {
        return walls[d.index];
    }

    /**
     * Compte le nombre de murs levés de la cellule.
     * @return nombre de murs levés de la cellule.
     */
    public int wallCount() {
        int i = 0;
        for (boolean b : walls) {
            i += b ? 1 : 0;
        }
        return i;
    }

    /**
     * Indique si la cellule est une impasse.
     * @return booléen disant si la cellule est une impasse.
     */
    public boolean isDeadEnd() {
        return wallCount() == 3;
    }

    /**
     * Renvoie le voisin de la cellule dans la direction d.
     * @param d direction dans laquelle on veut le voisin.
     * @return cellule voisine dans la direction indiquée.
     */
    public MazeCell getNeighbour(Direction d) {
        return maze.getCell(c + d.x, l + d.y);
    }

    /**
     * Renvoie un tableau-liste des voisins avec qui la cellule à été fusionnée.
     * @return tableau-liste des voisins avec qui la cellule à été fusionnée.
     */
    public ArrayList<MazeCell> getMergedNeighbours() {
        ArrayList<MazeCell> neighbours = new ArrayList<MazeCell>();
        for (Direction d : Direction.values()) {
            if (getNeighbour(d) != null && isMerged(d)) {
                neighbours.add(getNeighbour(d));
            }
        }
        return neighbours;
    }

    /**
     * Vérifie si la cellule peut fusionner avec sa voisine dans la direction d.
     * @param d direction dans laquelle on se pose la question.
     * @return booléen affirmant si oui ou non la cellule peut être fusionnée dans la direction d.
     */
    public boolean canMerge(Direction d) {
        return this.hasWall(d) && getNeighbour(d).hasWall(d.opp());
    }

    /**
     * Vérifie si la cellule est fusionnée avec sa voisine dans la direction d.
     * @param d direction dans laquelle on doit vérifier si la cellule est fusionnée
     * @return booléen affirmant si oui ou non la cellule est fusionnée dans la direction d.
     */
    public boolean isMerged(Direction d) {
        return !this.hasWall(d) && !getNeighbour(d).hasWall(d.opp());
    }

    /**
     * Fusionne la cellule avec sa voisine dans une direction donnée.
     * @param d direction à fusionner.
     */
    public void merge(Direction d) {
        if (canMerge(d) && !isMerged(d)) {
            lowerWall(d);
            getNeighbour(d).lowerWall(d.opp());
        }
    }

    //
    // Rendering
    //
    /**
     * Modifie la couleur du fond de la cellule.
     * @param c nouvelle couleur.
     */
    public void setBackgroundColor(Color c) {
        bg = c;
    }

    /**
     * Affiche la couleur du fond de la cellule.
     * @return la couleur du fond de la cellule.
     */
    public Color getBackgroundColor() {
        return bg;
    }

    /**
     * Affiche si l'objet est affiché ou non.
     *
     * @return si l'objet est modélisé ou non.
     */
    @Override
    public boolean isRendered() {
        return rendered;
    }

    /**
     * Modifie si l'objet est affiché ou non.
     *
     * @param yesno l'objet est affiché ou non.
     */
    @Override
    public void setRendered(boolean yesno) {
        rendered = yesno;
    }

    /**
     * Renvoie la position x à laquelle l'objet est affiché.
     *
     * @return la position x à laquelle l'objet est affiché.
     */
    @Override
    public int getRenderX() {
        return c * CELL_WIDTH;
    }

    /**
     * Renvoie la position y à laquelle l'objet est affiché.
     *
     * @return la position y à laquelle l'objet est affiché.
     */
    @Override
    public int getRenderY() {
        return l * CELL_HEIGHT;
    }

    /**
     * Effectue le rendu graphique de la cellule.
     *
     * @param gc le conteneur de jeu.
     * @param g le contexte graphique.
     * @throws SlickException
     */
    @Override
    public void render(GameContainer gc, Graphics g) {
        if (isRendered()) {

            Ressource.ground.draw(c * CELL_WIDTH, l * CELL_HEIGHT, CELL_WIDTH, CELL_HEIGHT);
            if (walls[Direction.NORTH.index]) {
                //g.drawLine(c * CELL_WIDTH, l * CELL_HEIGHT, c * CELL_WIDTH + CELL_WIDTH, l * CELL_HEIGHT);
                Ressource.wallHor.draw(c * CELL_WIDTH, l * CELL_HEIGHT, CELL_WIDTH, wallWidth);
            }
            if (walls[Direction.EAST.index]) {
                //g.drawLine(c * CELL_WIDTH + CELL_WIDTH, l * CELL_HEIGHT, c * CELL_WIDTH + CELL_WIDTH, l * CELL_HEIGHT + CELL_HEIGHT);
                Ressource.wallVert.draw(c * CELL_WIDTH + CELL_WIDTH, l * CELL_HEIGHT, wallWidth, CELL_HEIGHT);

            }
            if (walls[Direction.SOUTH.index]) {
                //g.drawLine(c * CELL_WIDTH, l * CELL_HEIGHT + CELL_HEIGHT, c * CELL_WIDTH + CELL_WIDTH, l * CELL_HEIGHT + CELL_HEIGHT);
                Ressource.wallHor.draw(c * CELL_WIDTH, l * CELL_HEIGHT + CELL_HEIGHT, CELL_WIDTH, wallWidth);
            }
            if (walls[Direction.WEST.index]) {
                //g.drawLine(c * CELL_WIDTH, l * CELL_HEIGHT, c * CELL_WIDTH, l * CELL_HEIGHT + CELL_HEIGHT);
                Ressource.wallVert.draw(c * CELL_WIDTH, l * CELL_HEIGHT, wallWidth, CELL_HEIGHT);
            }
            if (bg == Color.magenta) {
                g.drawOval(c * CELL_WIDTH + 10, l * CELL_HEIGHT + 10, CELL_WIDTH - 20, CELL_HEIGHT - 20);
            }

        }
    }

    /**
     * Active la phase cyclique de mise à jour + rendu graphique.
     */
    public void start() {
        setRendered(true);
    }

    /**
     * Inhibe la phase cyclique de mise à jour + rendu graphique.
     */
    public void stop() {
        setRendered(false);
    }

    /**
     * Indique la chaine de caractère à renvoyer lorqu'on appelle l'objet dans
     * une chaine de caractère.
     *
     * @return une chaine de caractère.
     */
    @Override
    public String toString() {
        return "( " + c + " , " + l + ")";
    }
}
