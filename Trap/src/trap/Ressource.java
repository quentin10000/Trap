package trap;

import java.util.HashMap;
import java.util.Map;

import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.UnicodeFont;
import org.newdawn.slick.font.effects.ColorEffect;

public class Ressource {

    /**
     * Images concernant les guis.
     */
    public static Image guiMainMenu;
    public static Image guiButtonPlayBase;
    public static Image guiButtonPlayHover;
    public static Image guiButtonCloseBase;
    public static Image guiButtonCloseHover;
    public static Image guiButtonPauseBase;
    public static Image guiButtonPauseHover;
    public static Image guiButtonMainBase;
    public static Image guiButtonMainHover;

    /**
     * Police de caractère.
     */
    public static UnicodeFont lemonMick;

    /**
     * Images concernant Trap.
     */
    public static Image wolf;
    public static Image mouse;
    public static Image player;
    public static Image playerHappy;
    public static Image playerDead;
    public static Image cheese3Part;
    public static Image cheese2Part;
    public static Image cheese1Part;
    public static Image ground;
    public static Image wallHor;
    public static Image wallVert;

    /**
     * Chargement des textures
     *
     * @throws SlickException Impossible de charger les textures
     */
    public static void loadTexture() throws SlickException {
        try {

            guiMainMenu = new Image("ressources/guiMainMenu.png");

            guiButtonPlayBase = new Image("ressources/play.png");
            guiButtonPlayHover = new Image("ressources/play_hover.png");

            guiButtonCloseBase = new Image("ressources/close.png");
            guiButtonCloseHover = new Image("ressources/close_hover.png");

            guiButtonPauseBase = new Image("ressources/pause.png");
            guiButtonPauseHover = new Image("ressources/pause_hover.png");

            guiButtonMainBase = new Image("ressources/mainMenu.png");
            guiButtonMainHover = new Image("ressources/mainMenu_hover.png");

            wolf = new Image("ressources/wolf2.png");
            mouse = new Image("ressources/mouse2.png");
            cheese3Part = new Image("ressources/cheese3.png");
            cheese2Part = new Image("ressources/cheese2.png");
            cheese1Part = new Image("ressources/cheese1.png");
            player = new Image("ressources/player2.png");
            playerHappy = new Image("ressources/player1.png");
            playerDead = new Image("ressources/player3.png");
            ground = new Image("ressources/ground.png");
            wallHor = new Image("ressources/wall.png");
            wallVert = new Image("ressources/wall_vert.png");

        } catch (SlickException e) {
            e.printStackTrace();
        }
    }

    /**
     * Chargement de la police de caractères
     *
     * @throws SlickException Impossible de charger la police de caractères
     */
    @SuppressWarnings("unchecked")
    public static void loadFont() throws SlickException {
        try {
            lemonMick = new UnicodeFont("ressources/LemonMilk.ttf", 40, false, false);
            lemonMick.addAsciiGlyphs();
            lemonMick.addGlyphs(400, 600);
            lemonMick.getEffects().add(new ColorEffect());
            lemonMick.loadGlyphs();

        } catch (SlickException e) {
            e.printStackTrace();
        }
    }
}
