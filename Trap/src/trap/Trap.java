package trap;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;

import entity.Cheese;
import entity.Mouse;
import entity.Player;
import entity.Wolf;
import gui.Gui;
import gui.GuiStart;
import maze.Maze;

/**
 *
 * @author meryd
 */
public class Trap extends BasicGame {

    /**
     * Longueur en pixel de la fenetre.
     */
    public static final int DISPLAY_WIDTH = 1300;

    /**
     * Hauteur en pixel de la fenetre.
     */
    public static final int DISPLAY_HEIGHT = 800;

    /**
     * Limite de fps.
     */
    public static final int FRAME_RATE = 60;

    /**
     * Nom de la fenetre.
     */
    public static final String GAME_TITLE = "Trap";

    /**
     * Longueur en pixel du jeu.
     */
    public static final int GAME_WIDTH = DISPLAY_WIDTH;

    /**
     * Hauteur en pixel du jeu.
     */
    public static final int GAME_HEIGHT = DISPLAY_HEIGHT;

    /**
     * Affichage des éléments de debug.
     */
    public static boolean DEBUG = false;

    /**
     * Le jeu est en pause.
     */
    private static boolean pause = true;

    /**
     * Score du jeu.
     */
    private static int score = 0;

    /**
     * Menu du principal.
     */
    private static Gui gui;

    /**
     * Contenu du jeu.
     */
    private static AppGameContainer app;

    /**
     * Labyrinthe du jeu.
     */
    private Maze maze;
    /**
     * Liste des souris du jeu.
     */
    private MouseSpawner sm;
    /**
     * Joueur.
     */
    private Player player;
    /**
     * Liste des fromages du jeu.
     */
    private CheeseSpawner sc;
    /**
     * Loup.
     */
    private Wolf wolf;
    
    /**
     * Création du jeu Trap.
     */
    public Trap() {
        super(GAME_TITLE);
    }

    /**
     * Lancement du jeu Trap.
     * @throws SlickException 
     */
    public static void start() throws SlickException {
        app = new AppGameContainer(new Trap());
        app.setDisplayMode(DISPLAY_WIDTH, DISPLAY_HEIGHT, false);
        app.setAlwaysRender(true);
        app.setIcons(new String[]{"ressources/icon16.png", "ressources/icon32.png", "ressources/icon64.png"});
        gui = new GuiStart(app);
        app.start();
    }

    /**
     * Initialisation du jeu Trap.
     * @param gc le conteneur de jeu.
     * @throws SlickException 
     */
    @Override
    public void init(GameContainer gc) throws SlickException {
        Ressource.loadTexture();
        Ressource.loadFont();
        gui.init(gc);
        gc.setTargetFrameRate(FRAME_RATE);
    }

    /**
     * Effectue la mise à jour du jeu Trap.
     *
     * @param gc le conteneur de jeu.
     * @param delta millisecondes écoulées depuis la dernière mise à jour.
     * @throws SlickException
     */
    @Override
    public void update(GameContainer gc, int delta) throws SlickException {
        gui.update(gc, delta);
    }

    /**
     * Effectue le rendu graphique du jeu Trap.
     *
     * @param gc le conteneur de jeu.
     * @param g le contexte graphique.
     * @throws SlickException
     */
    @Override
    public void render(GameContainer gc, Graphics g) throws SlickException {
        gc.setShowFPS(false);
        gui.render(gc, g);
    }

    /**
     * Mettre le jeux en pause
     *
     * @param bool mettre ou pas en pause
     */
    public static void setPause(boolean bool) {
        pause = bool;
    }

    /**
     * Teste si le jeux est en pause ou pas
     *
     * @return True : jeu en pause <br>
     * False : jeu n'est pas en pause
     */
    public static boolean isPaused() {
        return pause;
    }

}
