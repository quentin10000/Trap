package trap;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;

import entity.Cheese;
import maze.Maze;
import maze.MazeCell;

public class CheeseSpawner {

    /**
     * Liste des fromages qui sont dans le jeu.
     */
    private static List<Cheese> cheeses = new ArrayList<Cheese>();
    /**
     * Labyrinthe dans lequel sont les fromages.
     */
    private static Maze maze;
    /**
     * Controle du rendu graphique de tous les fromages.
     */
    private boolean render = true;

    /**
     * Construit la liste des fromages avec size fromages.
     * @param maze labyrinthe dans lequel seront créer les fromages
     * @param size nombre de fromages qui doivent être créer dans le labyrinthe
     */
    public CheeseSpawner(Maze maze, int size) {
        CheeseSpawner.maze = maze;
        for (int i = 0; i < size; i++) {
            // place les cellules dans une impasse aléatoire
            MazeCell cell = maze.getRandomDeadEnd();
            cheeses.add(new Cheese(maze, cell.c, cell.l));
        }
    }

    /**
     * Réduit le nombre de part du fromage à l'emplacement (x,y) du labyrinthe s'il existe
     * @param x colonne du labyrinthe
     * @param y ligne du labyrinthe
     */
    public static void eatCheese(int x, int y) {
        Cheese c = CheeseSpawner.getCheese(x, y);
        if (c != null) {
            if (c.getParts() > 0) {
                c.eat();
            } else {
                removeCheese(c);
            }
        }
    }

    /**
     * Retire le fromage cheese du labyrinthe et de la liste
     * @param cheese fromage à retirer
     */
    public static void removeCheese(Cheese cheese) {
        cheeses.remove(cheese);
    }

    /**
     * Choisis un fromage aléatoirement
     * @return le fromage
     */
    public static Cheese getRandCheese() {
        return cheeses.size() > 0 ? cheeses.get(cheeses.size() - 1) : null;
    }

    /**
     * Renvoie le fromage de la position (x,y) du labyrinthe s'il existe
     * @param x colonne du labyrinthe
     * @param y ligne du labyrinthe
     * @return le fromage correspondant s'il existe sinon rien
     */
    public static Cheese getCheese(int x, int y) {
        Iterator<Cheese> it = cheeses.iterator();
        while (it.hasNext()) {
            Cheese cheese = (Cheese) it.next();
            if (cheese.getCurrentCell().c == x && cheese.getCurrentCell().l == y) {
                return cheese;
            }
        }
        return null;
    }

    /**
     * Renvoie le fromage de la cellule cell du labyrinthe s'il existe
     * @param cell cellule du labyrinthe
     * @return le fromage correspondant s'il existe sinon rien
     */
    public static Cheese getCheese(MazeCell cell) {
        Iterator<Cheese> it = cheeses.iterator();
        while (it.hasNext()) {
            Cheese cheese = (Cheese) it.next();
            if (cheese.getCurrentCell() == cell) {
                return cheese;
            }
        }
        return null;
    }

    /**
     * Regarde si la liste continent le fromage de la position (x,y)
     * @param x colonne du labyrinthe
     * @param y ligne du labyrinthe
     * @return booléen si oui ou non la liste contient le fromage
     */
    public static boolean contain(int x, int y) {
        return cheeses.contains(new Cheese(maze, x, y));
    }

    /**
     * Effectue le rendu graphique de chaque fromage.
     *
     * @param gc le conteneur de jeu.
     * @param g le contexte graphique.
     * @throws SlickException
     */
    public void render(GameContainer gc, Graphics g) throws SlickException {
        if (isRendered()) {
            Iterator<Cheese> it = cheeses.iterator();
            while (it.hasNext()) {
                Cheese cheese = (Cheese) it.next();
                cheese.render(gc, g);
            }
        }
    }

    /**
     * Affiche si l'objet est affiché ou non.
     *
     * @return si l'objet est modélisé ou non.
     */
    public boolean isRendered() {
        return render;
    }

    /**
     * Modifie si l'objet est affiché ou non.
     *
     * @param yesno l'objet est affiché ou non.
     */
    public void setRendered(boolean yesno) {
        render = yesno;
    }

    /**
     * Active la phase cyclique de mise à jour + rendu graphique.
     */
    public void start() {
        setRendered(true);
    }

    /**
     * Inhibe la phase cyclique de mise à jour + rendu graphique.
     */
    public void stop() {
        setRendered(false);
        cheeses.clear();
    }

}
