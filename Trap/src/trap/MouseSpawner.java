package trap;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;

import entity.Mouse;
import maze.Maze;
import maze.MazeCell;
import util.Renderable;

public class MouseSpawner {

    /**
     * Liste des souris qui sont dans le jeu.
     */
    private static List<Mouse> mice = new ArrayList<Mouse>();
    /**
     * Labyrinthe dans lequel sont les souris.
     */
    private static Maze maze;
    /**
     * Controle du rendu graphique de toutes les souris.
     */
    private boolean render = true;
    /**
     * Controle de la mise a jour de toutes les souris.
     */
    private boolean update = true;

    /**
     * Construit la liste des fromages avec size fromages.
     * @param maze labyrinthe dans lequel seront créer les fromages
     * @param size nombre de fromages qui doivent être créer dans le labyrinthe
     */
    public MouseSpawner(Maze maze, int size) {
        MouseSpawner.maze = maze;
        for (int i = 0; i < size; i++) {
            MazeCell cell = maze.getRandomDeadEnd();
            mice.add(new Mouse(maze, cell.c, cell.l));
        }

    }

    /**
     * Retire la souris à la position (x,y) du labyrinthe et de la liste si elle existe
     * @param x colonne du labyrinthe
     * @param y ligne du labyrinthe
     */
    public static void removeMouse(int x, int y) {
        Mouse m = MouseSpawner.getMouse(x, y);
        if (m != null) {
            removeMouse(m);
        }
    }

    /**
     * Retire la souris mouse du labyrinthe et de la liste
     * @param mouse souris à retirer
     */
    public static void removeMouse(Mouse mouse) {
        mice.remove(mouse);
    }

    /**
     * Choisis une souris aléatoirement
     * @return la souris
     */
    public static Mouse getRandMouse() {
        return mice.size() > 0 ? mice.get(mice.size() - 1) : null;
    }

    /**
     * Renvoie la souris de la position (x,y) du labyrinthe si elle existe
     * @param x colonne du labyrinthe
     * @param y ligne du labyrinthe
     * @return la souris correspondante si elle existe sinon rien
     */
    public static Mouse getMouse(int x, int y) {
        Iterator<Mouse> it = mice.iterator();
        while (it.hasNext()) {
            Mouse mouse = (Mouse) it.next();
            if (mouse.getCurrentCell().c == x && mouse.getCurrentCell().l == y) {
                return mouse;
            }
        }
        return null;
    }

    /**
     * Renvoie la souris de la cellule cell du labyrinthe si elle existe
     * @param cell cellule du labyrinthe
     * @return la souris correspondante si elle existe sinon rien
     */
    public static Mouse getMouse(MazeCell cell) {
        Iterator<Mouse> it = mice.iterator();
        while (it.hasNext()) {
            Mouse mouse = (Mouse) it.next();
            if (mouse.getCurrentCell() == cell) {
                return mouse;
            }
        }
        return null;
    }

    /**
     * Regarde si la liste continent la souris de la position (x,y)
     * @param x colonne du labyrinthe
     * @param y ligne du labyrinthe
     * @return booléen si oui ou non la liste contient la souris
     */
    public static boolean contain(int x, int y) {
        return mice.contains(new Mouse(maze, x, y));
    }

    /**
     * Effectue le rendu graphique de chaque souris.
     *
     * @param gc le conteneur de jeu.
     * @param g le contexte graphique.
     * @throws SlickException
     */
    public void render(GameContainer gc, Graphics g) throws SlickException {
        if (isRendered()) {
            Iterator<Mouse> it = mice.iterator();
            while (it.hasNext()) {
                Mouse mouse = (Mouse) it.next();
                mouse.render(gc, g);
            }
        }
    }

    public void update(GameContainer gc, int delta) throws SlickException {
        if (isUpdated()) {
            Iterator<Mouse> it = mice.iterator();
            while (it.hasNext()) {
                Mouse mouse = (Mouse) it.next();
                mouse.update(gc, delta);
            }
        }
    }

    /**
     * Affiche si l'objet est affiché ou non.
     *
     * @return si l'objet est modélisé ou non.
     */
    public boolean isRendered() {
        return render;
    }

    /**
     * Modifie si l'objet est affiché ou non.
     *
     * @param yesno l'objet est affiché ou non.
     */
    public void setRendered(boolean yesno) {
        render = yesno;
    }

    /**
     * Affiche si l'objet est mis à jour ou non.
     *
     * @return si l'objet est mis à jour ou non.
     */
    public boolean isUpdated() {
        return update;
    }

    /**
     * Modifie si l'objet est mis à jour ou non.
     *
     * @param yesno l'objet est mis à jour ou non.
     */
    public void setUpdated(boolean yesno) {
        update = yesno;
    }

    /**
     * Active la phase cyclique de mise à jour + rendu graphique.
     */
    public void start() {
        setRendered(true);
        setUpdated(true);
    }

    /**
     * Inhibe la phase cyclique de mise à jour + rendu graphique.
     */
    public void stop() {
        setRendered(false);
        setUpdated(false);
        mice.clear();
    }

}
